#include "gestion_cli.h"
#include <iostream>
#include <stdlib.h>
#include <cstring>

using namespace std;

const char *argp_program_version = "valenquen 1.0";
const char *argp_program_bug_address = "<quentin.pipelier@bts-malraux.net>";

argp_option host = {"host", 'h', "Host FQDN", 0, "Documentation pour l'option 'host'", 0};
argp_option dbname = {"dbname", 'd', "DBname", 0, "", 0};
argp_option user = {"user", 'u', "User", 0, "", 0};
argp_option password = {"password", 'p', "Password", 0, "", 0};
argp_option nullopt = {0, 0, 0, 0, 0, 0};

static struct argp_option options[] =
{
  host,
  dbname,
  user,
  password,
  nullopt
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  struct arguments *mes_donnees = static_cast<struct arguments *>(state->input);

  switch(key)
  {
    case 'h':
      mes_donnees->host = arg;
      break;

    case 'd':
      mes_donnees->dbname = arg;
      break;

    case 'u':
      mes_donnees->user = arg;
      break;

    case 'p':
      mes_donnees->password = arg;
      break;

    case ARGP_KEY_ARG:
      if(state->arg_num >= 5)
      {
        argp_usage(state);
      }

      break;

    default:
      return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static char args_doc[] = "HOST DBNAME USER PASSWORD";

static char doc[] = "valenquen -- Programme libpq avec possibilite arguments";

static struct argp argp_options = { options, parse_opt, args_doc, doc, nullptr, nullptr, nullptr };

struct arguments appelArgp(int nbrParametres, char **tableauParam)
{
  std::string host = "postgresql.bts-malraux72.net";
  std::string dbname = "v.tazir";
  std::string user = "v.tazir";
  struct arguments options;
  options.host = static_cast<char *>(calloc(host.size() + 1, sizeof(char)));
  std::strcpy(options.host, host.c_str());
  options.dbname = static_cast<char *>(calloc(dbname.size() + 1, sizeof(char)));
  std::strcpy(options.dbname, dbname.c_str());
  options.user = static_cast<char *>(calloc(user.size() + 1, sizeof(char)));
  std::strcpy(options.user, user.c_str());
  options.password = nullptr;
  argp_parse(&argp_options, nbrParametres, tableauParam, 0, 0, &options);
  return options;
}
