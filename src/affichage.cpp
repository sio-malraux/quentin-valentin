#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <string>

#include "affichage.h"
#include "calcul.h"

using namespace std;

void AfficheInfos(PGconn *connexion)
{
  char *host = PQhost(connexion);
  char *user = PQuser(connexion);
  char *password = PQpass(connexion);
  char *database = PQdb(connexion);
  char *port = PQport(connexion);
  const char *encodage = PQparameterStatus(connexion, "server_encoding");
  int protocole = PQprotocolVersion(connexion);
  int version = PQserverVersion(connexion);
  int SSL = PQsslInUse(connexion);
  int versionbib = PQlibVersion();
  std::cout << "La connexion au serveur de base de données '" << host << "' à été établie avec les paramètres suivants : " << std::endl;
  std::cout << " * utilisateur : " << user << std::endl;
  std::cout << " * mot de passe : ";

  for(unsigned int i = 1 ; i <= std::strlen(password) ; i++)
  {
    std::cout << "*";
  }

  std::cout << std::endl << " * base de donnees : " << database << std::endl << " * port TCP : " << port << std::endl << " * chiffremment SSL : ";

  if(SSL == 1)
  {
    std::cout << "true";
  }
  else
  {
    std::cout << "false";
  }

  std::cout << std::endl << " * encodage : " << encodage << std::endl << " * version du protocole : " << protocole << std::endl << " * version du serveur : " << version << std::endl << " * version de la bibliothèque 'libpq' du client : " << versionbib << std::endl ;
}



void affichage_entete(PGresult *requetes, unsigned int nbCaractere, unsigned  int nbColonnes)
{
  for(unsigned int p = 0; p < nbColonnes; p++) //Affichage de l'en-tête
  {
    std::cout << std::left << std::setw(nbCaractere) << PQfname(requetes, p) << " | ";
  }
}

void affichage_donnees(PGresult *requetes, unsigned int nbCaractere, unsigned int nbColonnes, unsigned int nbResult)
{
  for(unsigned int l = 0; l < nbResult; l++)
  {
    std::cout << "| ";

    for(unsigned int c = 0; c < nbColonnes; c++)
    {
      std::cout << std::left << std::setw(nbCaractere) << supprimmeChaine(PQgetvalue(requetes, l, c), nbCaractere);
      std::cout << " | ";
    }

    std::cout << std::endl;
  }
}

void affichage_tiret(unsigned int tiret)
{
  for(unsigned int x = 0; x < tiret; x++)
  {
    std::cout << "-";
  }
}

void AfficheErreur(ExecStatusType reussite, char *status_requete)
{
  //Question 5
  if(reussite == PGRES_FATAL_ERROR)
  {
    std::cerr << "Erreur Fatale : " << status_requete << std::endl;
  }
  else if(reussite == PGRES_BAD_RESPONSE)
  {
    std::cerr << "Erreur : La réponse du serveur n'a pas été comprise : " << status_requete << std::endl;
  }
  else if(reussite == PGRES_EMPTY_QUERY)
  {
    std::cerr << "Erreur : Aucune requête : " << status_requete << std::endl;
  }
  else if(reussite == PGRES_NONFATAL_ERROR)
  {
    std::cerr << "Erreur non Fatale : " << status_requete << std::endl;
  }
  else
  {
    std::cerr << "Début de l'envoi ou de la réception de données : " << status_requete << std::endl;
  }
}
