#ifndef GESTIONCLI_H
#define GESTIONCLI_H

#include <iostream>
#include <argp.h>

struct arguments
{
  char *host,
       *dbname,
       *user,
       *password;
};

static error_t parse_opt(int key, char *argv, struct argp_state *state);

struct arguments appelArgp(int nbrParametres, char **tableauParams);
#endif
