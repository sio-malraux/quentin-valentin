#ifndef CALCUL_H
#define CALCUL_H

#include <iostream>
#include <libpq-fe.h>
using namespace std;

/** \brief Calcule le nombre de caractère necessaire pour afficher les tirets
 *
 * Calcule le nombre de nombre de caractère nécéssaire pour afficher les tirets en comptant le nombre de caractère total des champs de l'en tête
 *
 * \param nbrColonnes \c unsigned \c int Le nombre de champs \c
 * \param nbSeparation \c unsigned \c int Le nombre d'espace pour séparer les champs
 * \param nbCaractere \unsigned \c int Le nombre de caractère des champs
 * \param requete \c PGresult \c * La requete SQL
 *
 * \return nbSeparation \c unsigned \c int Le nombre de caractère necessaire pour afficher les tirets
 *
 */
unsigned int separationTiret(unsigned int nbrColonnes, unsigned int nbSeparation, unsigned int nbCaractere, PGresult *requete);

/** \brief Calcule le nombre de caractère le plus grand
 *
 * Calcule le nombre de nombre de caractère du champ de l'en tête le plus grand
 *
 * \param nbCaractere \c unsigned \c int Le nombre de caractère du champ le plus grand
 * \param nbrColonnes \c unsigned \c int Le nombre de champs \c
 * \param requete \c PGresult \c * La requete SQL
 *
 * \return nbCaractere \c unsigned \c int Le nombre de caractère du champ le plus grand de l'en tête
 *
 */
unsigned int calculNombreCaractere(unsigned int nbCaractere, unsigned int nbrColonnes, PGresult *requete);

/** \brief Tronque ou non la chaîne de caractère d'un tuple en fonction du champ de l'en tête le plus grand
 *
 * Si la taille du champ est supérieur à celle du champ le plus long de l'en tête, alors le champ sera tronqué et '...' sera affiché
 *
 * \param chaine \c char \c *  tableau de caractère du tuple
 * \param nbCaractere \c unsigned \c int Le nombre de caractère du champ de l'en tête le plus grand
 *
 * \return output \c string Une chaîne de caractère, tronquée ou non
 *
 */
string supprimmeChaine(char *chaine, unsigned int maxCaractere);

#endif // CALCUL_H
